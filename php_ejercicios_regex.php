<?php

//Realizar una expresión regular que detecte emails correctos.

function validarCorreo($email){

$regex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";

echo preg_match($regex, $email);
}


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.

function validarCurp($curp){
    $regex = "/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/";

    echo preg_match($regex, $curp);

}



//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.

function validarPalabra($regex, $palabra){

    $regex = "/^([a-zA-Z]{0,50})$/";

    echo preg_match($regex, $palabra);

}


validarPalabra($regex, $palabra);

//Crea una funcion para escapar los simbolos especiales.

function escapar($cadena){

    $cadena = preg_quote($cadena, '/');

}


//Crear una expresion regular para detectar números decimales.

function detectarDecimal($regex, $decimal){

    $regex = "/^[0-9]\.[0-9]{1,2}$";

    echo preg_match($regex, $decimal);

}


?>