<html>
<head>
    <title>Formulario</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="estilos.css">
</head>

<body>
    <h1 class="tituloPrincipal">Formulario de Registro</h1>  
        <nav>
            <ul>
                <li><a href="login.php">Login</a></li>
                <li><a href="informacion.php">Información</a></li>
                <li><a href="formulario.php">Registro</a></li>
    
            </ul>
        </nav>

        <h2 class="subtitulo">Registro de datos</h2>

       

        <div class="contenidoInscripciones">

           <center><p class="instrucciones"> Ingresa los campos solicitados para realizar registro de alumno.</p></center>
            <?php
            include("registrar.php");
            ?>
            <section>
                 <!--Creación de formulario de inscripción con PHP-->
                <form action ="registrar.php" method="post" class="formularioInscripcion" name="registro_alumno">
                    <table><tr><td>
                    
                    Numero de cuenta:</td><td><input type="text" name="num_cta_nva" required></td></tr>
                    <tr><td>
                    Nombre:</td><td><input type="text" name="nombre" maxlength="30" required></td></tr>
                    <tr><td>
                    Apellido Paterno:</td><td><input type="text" name="primer_apellido" maxlength="30" required></td></tr>
                    <tr><td>
                    Apellido Materno:</td><td><input type="text" name="segundo_apellido" maxlength="30" required></td></tr>
                    <tr><td>
                    Género:</td><td> 
                    <label class="form-radio"> <input type="radio" name="genero" value="H" checked><i class="form-icon"></i> Hombre</label>
                    <label class="form-radio"> <input type="radio" name="genero" value="M"> <i class="form-icon"></i> Mujer</label>
                    <label class="form-radio"> <input type="radio" name="genero" value="Otro"> <i class="form-icon"></i> Otro</label>
                    </td></tr>
                    <tr><td>
                    Fecha de nacimiento:&nbsp;</td><td><input type="date" name="fechaNacimiento" required></td></tr>
                    <tr><td>
                    Contraseña:</td><td><input name="password" input type="password" maxlength="30" required></td></tr>
                    <tr><td>
                    <tr><td>
                    </td></tr>
                    <tr><td>
                   
                    <input type="submit" name="btnEnviar" id="Enviar" value="Registrar"></td></tr>
            
                  </table>
                  </form>


            </section>

            <?php
            //echo "Usuario creado con exito"
            

            ?>

        </div>
</body>